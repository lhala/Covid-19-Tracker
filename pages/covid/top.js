import { Doughnut } from 'react-chartjs-2'
import { useState } from 'react'
import { Button } from 'react-bootstrap'
import toNum from '../../helpers/toNum'
import Head from 'next/head'

export default function top({convertedData}){

    const countryStatus = convertedData.countries_stat

    const [state, setState] = useState("Highest")
    
    const country = countryStatus.map(countryStat => {

        return {
            name: countryStat.country_name,
            cases: toNum(countryStat.cases)
        }
    })

    if (state==="Highest") {
        country.sort((firstEl, secondEl) => {
            if (firstEl.cases < secondEl.cases) {
                return 1
            } else if (firstEl.cases > secondEl.cases) {
                return -1
            } else {
                return 0
            }
        })
    } else {
        country.sort((firstEl, secondEl) => {
            if (firstEl.cases > secondEl.cases) {
                return 1
            } else if (firstEl.cases < secondEl.cases) {
                return -1
            } else {
                return 0
            }
        })
    }

    const toggle = (e) => {
        e.preventDefault()

        if (state==="Highest"){
            setState("Lowest")
        } else {
            setState("Highest")
        }

    }

    const buttonLabel = (state==="Highest") ? "Show Lowest" : "Show Highest"

    return(
        <>
            <Button onClick={(e)=>toggle(e)}>{buttonLabel}</Button>
            <Head>
                <title>COVID-19 Top Coutries</title>
            </Head>
          <h1 className="mt-5"> Top 10 Countries with the {state} Number of COVID-19 Cases</h1>
          <Doughnut data={
            {
                labels: [country[0].name, country[1].name, country[2].name, country[3].name, country[4].name, country[5].name, country[6].name, country[7].name, country[8].name, country[9].name],
                datasets: [{
                    data: [country[0].cases, country[1].cases, country[2].cases, country[3].cases, country[4].cases, country[5].cases, country[6].cases, country[7].cases, country[8].cases, country[9].cases],
                    backgroundColor: ["#cb0200", "#f50300", "#ff1f1c", "#ff403e", "#ff5b59", "#f8662f", "#f97a4a", "#f98255", "#fa9169", "#fbab8c"]  
                }]
            }
          }/>
        </>
    )
}

export async function getStaticProps() {

    const response = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
            "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })

    const convertedData = await response.json()

    return {
        props: {
            convertedData
        } 
    }

}