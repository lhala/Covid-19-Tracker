import Head from 'next/head'
import styles from '../styles/Home.module.css'
import toNum from '../helper/toNum'
import Jumbotron from 'react-bootstrap/Jumbotron' 

export default function Home({globalTotal}) {
  return (
    <div>
      <Head>
        <title>Covid-19 Tracker App</title>
      </Head>

        <Jumbotron>
            <h1>Total Covid-19 cases in the world:
                <strong>{globalTotal.cases}</strong>
            </h1>
        </Jumbotron>
    
    </div>
  )
}

export async function getStaticProps() {
  //fetch data from our API endpoint
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
  "method": "GET",
  "headers": {
    "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
    "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
  }
})
  const convertedData = response.json()
  
}
