import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'

export default function MyApp({ Component, pageProps }) {
  return (
  	<>
  	<NavBar />
    <Container>
    	<Component {...pageProps} />
    </Container>
    </>
  	)    
}

