import { Navbar, Nav, Container } from 'react-bootstrap';
import Link from 'next/link'; 

//lets create a function that will describe the structure of this component.
export default function NavBar() {
	return(
  
		<Navbar  bg="warning" className= "light" expand="lg">
      <Container className="pr-3">
           <Link href="/">
              <a className="navbar-brand"> COVID-19 Tracker</a>
           </Link>
           <Navbar.Toggle aria-controls="basic-navbar-nav" />
           <Navbar.Collapse id="basic-navbar-nav">
           	   <Nav className="mr-auto">
           	   	  {/* top infected countries with COVID-19*/}
           	   	  <Link href="/covid/top">
              		<a className="nav-link"> Top Countries</a>
           		  </Link>
           	   	  {/* Find a Country to see number of COVID-19 cases*/}
           	   	  <Link href="/covid/search">
           			   <a className="nav-link"> Find a Country</a>
         		  </Link>
           	   </Nav>
           </Navbar.Collapse>
            </Container>
		</Navbar>
   
	)
}