//function for converting string API results to numbers
export default function toNum(str) {
   //convert string to array to gain access to array methods. 
   const arr = [...str]
   //filtered out the commas in the string
   const elementNaWalangComma = arr.filter(element => element !== ",")
   //reduced the filtered array back to a single string without the commas
   /*["" "" ""]*/
   //the string is now then converted into a number using parseInt()
   return parseInt(elementNaWalangComma.reduce((firstElement,nextElement) => firstElement + nextElement)) 
}